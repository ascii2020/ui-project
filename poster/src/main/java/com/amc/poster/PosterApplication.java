package com.amc.poster;

import com.amc.javafx.core.JavaFxApplication;
import com.amc.poster.constants.PosterConstant;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@ComponentScan
public class PosterApplication {

    /**
     * 该程序需要以管理员身份运行
     */
    public static void main(String[] args) {
        PosterConstant.loadData();
        JavaFxApplication.run(PosterApplication.class, args);
    }

}
